# Sublime Overlord Package

## SQL Ex.tmLanguage Syntaxes

### Dark - Cobalt Ex.tmTheme
![syntax_sql_ex_dark](https://bytebucket.org/DarkOverlord/sublime_overlord/raw/master/images/syntax_sql_ex_dark.png)

### Light - Solarized Ex (Light).tmTheme
![syntax_sql_ex_light](https://bytebucket.org/DarkOverlord/sublime_overlord/raw/master/images/syntax_sql_ex_light.png)

### Visual Studio Ex.tmTheme
![syntax_sql_ex_light](https://bytebucket.org/DarkOverlord/sublime_overlord/raw/master/images/syntax_sql_ex_light2.png)

## Links

- [Sublime Overlord Main](https://bitbucket.org/DarkOverlord/sublime_overlord)
- [Sublime Overlord FORIS Package](https://bitbucket.org/DarkOverlord/sublime_overlord_foris) (private)
- [Sublime Overlord Settings Package](https://bitbucket.org/DarkOverlord/sublime_overlord_settings) (private)
